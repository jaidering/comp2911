package lect05;

import java.util.*;

public class ListSetConversion
{
    
    public static void main (String [] args)
    {
        List <String> list = Arrays.asList ("cat", "aardvark", "zebra", "zoo",
                "mouse", "cat");
        display ("data as list ", list);
        
        List <String> sortedList = new ArrayList <String> (list); // copy of list
        Collections.sort (sortedList);
        display ("sorted list ", sortedList);
        
        Set <String> hashSet = new HashSet <String> (list);
        display ("data as set (duplicates removed) ", hashSet);
        
        Set <String> treeSet = new TreeSet <String> (list);
        display ("data as sorted set (duplicates removed) ", treeSet);
        
        List <String> treeList = new ArrayList <String> (treeSet);
        display ("sorted set converted back to list ", treeList);
        
    }
    
    static void display (String prefix, Collection <?> data)
    {
        System.out.println (prefix);
        System.out.println (data);
        System.out.println ();
    }
    
}
