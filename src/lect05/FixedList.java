package lect05;

import java.util.Arrays;

/** A fixed size list. Entries may be updated.
 */
public class FixedList <E>
{
    
    E [] items;
    
    /**
     * Initialise fixed list to given size. All entries are null.
     * 
     * @param n
     *            size of list
     */
    @SuppressWarnings ("unchecked")
    public FixedList (int n)
    {
        items = (E []) new Object [n]; // type cast is OK because all entries
                                       // are null
    }
    
    FixedList (FixedList <E> fixedList)
    {
        items = fixedList.items;
    }
    
    public int size ()
    {
        return items.length;
    }
    
    public void set (int i, E item)
    {
        items [i] = item;
    }
    
    public E get (int i)
    {
        return items [i];
    }
    
    public FixedList <E> copy ()
    {
        return new FixedList <E> (this);
    }
    
    public FixedList <E> deepCopy ()
    {
        FixedList <E> result = new FixedList <E> (this);
        result.items = Arrays.copyOf (items, items.length);
        return result;
    }
    
    @Override
    public boolean equals (Object o) {
    	return o instanceof FixedList && ((FixedList<?>) o).equals(this);
    }
    
    public boolean equals (FixedList<?> list) {
    	// in Java array1.equals(array2) only when the arrays are the same
    	// so we need to use the utility for comparing array elements in java.util.Arrays
    	return Arrays.equals(items, list.items); 
    }
}
