package lect05;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FixedListTest
{
    
    FixedList <Object> testList;
    Object             item;
    int                index = 9;
    FixedList <Object> otherTestList;
    
    FixedList <Object> make (int n)
    {
        return new FixedList <Object> (n);
    }
    
    @Before
    public void setUp () throws Exception
    {
        testList = make (10);
        item = new Object ();
        testList.set (index, item);
    }
    
    @Test
    public void testSet ()
    {
        assertEquals (item, testList.get (index));
    }
    
    @Test
    public void testCopyEqualsButNotSame ()
    {
        otherTestList = testList.copy ();
        item = otherTestList.get (index);
        assertEquals (item, testList.get (index));
        assertEquals (item, otherTestList.get (index));
        assertNotSame (testList, otherTestList);
        assertEquals (testList, otherTestList);
    }
        
    @Test
    public void testCopyIsShallow ()
    {
        otherTestList = testList.copy ();
        item = otherTestList.get (index);
        // add new item to one list and check it appears in the copy
        Object newItem = new Object ();
        assertNotSame (item, newItem);
        testList.set (index, newItem);
        assertEquals ("test list contains new item", newItem, testList.get (index));
        assertEquals ("copied list contains new item test list update", newItem, otherTestList.get (index));
        assertTrue ("testList.equals(otherTestList)", testList.equals(otherTestList));
    }
    
    @Test
    public void testDeepCopyEqualsButNotSame ()
    {
        otherTestList = testList.deepCopy ();
        item = otherTestList.get (index);
        assertEquals (item, testList.get (index));
        assertEquals (item, otherTestList.get (index));
        assertEquals (testList, otherTestList);
        assertNotSame (testList, otherTestList);
        assertEquals (testList, otherTestList);
    }
   
    @Test
    public void testDeepCopyIsDeep ()
    {
        otherTestList = testList.deepCopy ();
        item = otherTestList.get (index);
        // add new item to list and check that it does not appear in deep copy
        Object newItem = new Object ();
        testList.set (index, newItem);
        assertNotSame (testList, otherTestList);
        assertEquals ("test list contains new item", newItem, testList.get (index));
        assertEquals ("test list contains old item", item, otherTestList.get (index));
        assertFalse ("testList.equals(otherTestList)", testList.equals(otherTestList));
    }
    
    void display ()
    {
        System.out.println ("item = " + item);
        System.out.println ("index = " + index);
        System.out.println ("testList.get(index) = " + testList.get (index));
        System.out.println ("otherTestList.get(index) = "
                            + otherTestList.get (index));
        
    }
}
