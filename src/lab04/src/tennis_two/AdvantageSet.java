package tennis_two;

public class AdvantageSet extends SerialContest implements Set{

	public Player server = firstServer;
	protected AdvantageSet(Player firstServer, int targetScore, int advantage) {
		super(firstServer, targetScore, advantage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getContestName() {
		// TODO Auto-generated method stub
		return "AdvantageSet";
	}

	@Override
	protected Contest makeSubContest() {
		// TODO Auto-generated method stub
		if ((score_1 + score_2) % 2 == 0)
            server = firstServer;
        else
           server  = players ().other (firstServer);
		return  TennisFactory.instance().makeSinglesGame(server);
	}

}
