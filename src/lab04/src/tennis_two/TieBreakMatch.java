package tennis_two;

public class TieBreakMatch extends AbstractMatch {
	Player server = firstServer;
	protected TieBreakMatch(Player firstServer, int targetScore, int advantage) {
		super(firstServer, targetScore, advantage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getContestName() {
		// TODO Auto-generated method stub
		return "TieBreakMatch";
	}

	@Override
	protected Contest makeSubContest() {
		// TODO Auto-generated method stub
		
		if (currentSubContest != null) {
			if ((currentSubContest.score(players()._1) + currentSubContest.score(players()._2)) % 2 == 0) {
				server = currentSubContest.server();
			} else {
				server = players ().other (currentSubContest.server());
			}
		}
		return TennisFactory.instance ().makeTieBreakSet(server);
	}

}