package tennis_two;

public class AdvantageMatch extends AbstractMatch{
    public Player server = firstServer;
	protected AdvantageMatch(Player firstServer, int targetScore, int advantage) {
		super(firstServer, targetScore, advantage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getContestName() {
		// TODO Auto-generated method stub
		return "AdvantageMatch";
	}

	@Override
	protected Contest makeSubContest() {	
		// TODO Auto-generated method stub
		
	if ( currentSubContest != null) {
		if ((currentSubContest.score(players()._1) + currentSubContest.score(players()._2)) % 2 == 0) {
				server = currentSubContest.server();
			} else {
				server = players ().other (currentSubContest.server());
			}
		}
	return  TennisFactory.instance().makeAdvantageSet(server);
	}
}
