package tennis_two;

public abstract class AbstractMatch extends SerialContest implements Match {

	int [] AllScoreForPlayer1 = new int [targetScore * 2 - 1];
	int [] AllScoreForPlayer2 = new int [targetScore * 2 - 1];
	int setCounter = 0;
	
	protected AbstractMatch(Player firstServer, int targetScore, int advantage) {
		super(firstServer, targetScore, advantage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public abstract String getContestName();

	@Override
	protected abstract Contest makeSubContest();
	
	@Override
	protected void overridableDisplay ()
    {
        AllScoreForPlayer1[setCounter] = currentSubContest.score(players()._1);
        AllScoreForPlayer2[setCounter++] = currentSubContest.score(players()._2);
 
        System.out.format ("%-15s", players()._1);
        for (int i = 0; i < setCounter; i++) {
        	System.out.format ("%3s", AllScoreForPlayer1[i]);
        }
        System.out.print('\n');
        System.out.format ("%-15s", players()._2);
        for (int i = 0; i < setCounter; i++) {
        	System.out.format ("%3s", AllScoreForPlayer2[i]);
        }
        System.out.print('\n');
    }

}