package tennis_two;

public class TieBreakSet extends SerialContest implements Set {
	Player server = firstServer;
	protected TieBreakSet(Player firstServer, int targetScore, int advantage) {
		super(firstServer, targetScore, advantage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getContestName() {
		// TODO Auto-generated method stub
		return "TieBreakSet";
	}
/**
 * 
 */
	@Override
	protected Contest makeSubContest() {
		// TODO Auto-generated method stub
		
		if (currentSubContest != null) {
			server = players ().other (currentSubContest.server());
		}
		if (score_1 == 6 && score_2 == 6) {
			return TennisFactory.instance ().makeTieBreakGame(server);
		} else {
			return TennisFactory.instance ().makeSinglesGame(server);
		}
	}

	@Override
	public boolean isOver () {
		return (Math.max (score_1, score_2) >= targetScore && Math.abs (score_1 - score_2) >= targetAdvantage) 
				|| (score_1 == 6 && score_2 == 7) 
				|| (score_1 == 7 && score_2 == 6);
	}

	
}
