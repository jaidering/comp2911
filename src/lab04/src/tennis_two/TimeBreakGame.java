package tennis_two;

public class TimeBreakGame extends SerialContest implements Game {

	protected TimeBreakGame(Player firstServer, int targetScore, int advantage) {
		super(firstServer, targetScore, advantage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getContestName() {
		// TODO Auto-generated method stub
		return "TieBreakGame";
	}

	@Override
	protected Contest makeSubContest() {
		// TODO Auto-generated method stub
		if ((score_1 + score_2 + 1) % 4 < 2) {
			return TennisFactory.instance ().makePoint(firstServer);
		} else {
			return TennisFactory.instance ().makePoint(players ().other (firstServer));
		}
	}

}