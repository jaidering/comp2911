package lab12;

public class LongestCommonSubsequence {
	public String solve(String s, String t) {
		int[][] lengths = new int[s.length()+1][t.length()+1];

		// row 0 and column 0 are initialized to 0 already

		for (int i = 0; i < s.length(); i++)
			for (int j = 0; j < t.length(); j++)
				if (s.charAt(i) == t.charAt(j))
					lengths[i+1][j+1] = lengths[i][j] + 1;
				else
					lengths[i+1][j+1] =
					Math.max(lengths[i+1][j], lengths[i][j+1]);

		// read the substring out from the matrix
		StringBuffer sb = new StringBuffer();
		for (int x = s.length(), y = t.length();
				x != 0 && y != 0; ) {
			if (lengths[x][y] == lengths[x-1][y])
				x--;
			else if (lengths[x][y] == lengths[x][y-1])
				y--;
			else {
				assert s.charAt(x-1) == t.charAt(y-1);
				sb.append(s.charAt(x-1));
				x--;
				y--;
			}
		}
		return sb.reverse().toString();
	}
}
