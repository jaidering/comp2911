package quoridor;
import java.util.LinkedList;
import java.util.List;


public class Tiles {

	int xAxis;
	int yAxis;

	public Tiles() {
		//do nothing
	}

	public Tiles(int x, int y) {
		this.xAxis = x;
		this.yAxis = y;
	}

	public Tiles(String s) {
		if (s.length() == 2) {
			//players moves
			this.yAxis = s.charAt(0) - 'a';
			this.xAxis = s.charAt(1) - '1';
		}
		else {
			//Check for input that is not of length 2
		}
	}

	public int getColumn() {
		return this.xAxis;
	}

	public int getRow() {
		return this.yAxis;
	}

	public List <Tiles> possibleMoves (int numOfTiles) {
		List <Tiles> possibleMoves = new LinkedList<Tiles>();

		for (int i = -numOfTiles; i < numOfTiles+1; i++) {
			if (i != 0) {
				int newRow = yAxis + i;
				int newCol = xAxis + i;
				if (newRow >= 0 && newRow < 9) {
					possibleMoves.add(new Tiles(newRow, xAxis));
				}
				if (newCol >= 0 && newCol < 9) {
					possibleMoves.add(new Tiles(yAxis, newCol));
				}
			}
		}
		return possibleMoves;
	}

	@Override
	public String toString() {
		int row = this.xAxis;
		int column = this.yAxis;
		return ""+column+row;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Tiles) {
			Tiles t = (Tiles)o;
			if (t.xAxis==xAxis && t.yAxis==yAxis) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return 9*xAxis+yAxis;
	}
} 