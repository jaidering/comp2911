package quoridor;
import java.util.Iterator;
import java.util.List;
 
public class Validator {
 
    // TODO complete this class using your project code
    // you must implement the no-arg constructor and the check method
     
    // you may add extra fields and methods to this class
    // but the ProvidedTests code only calls the specified methods
     
    public Validator() {
        // TODO
    }
 
    /**
     * Check the validity of a given list of moves.
     * Each move is represented by a string.
     * The list is valid if and only if each move in the list is valid,
     * after applying the preceding moves in the list, assuming they are valid,
     * starting from the initial position of the game.
     * When the game has been won, no further moves are valid.
     * @param moves a list of successive moves
     * @return validity of the list of moves
     */
    public boolean check(List<String> moves) {
        boolean valid = true;
        Board game = new Board();
        
        Iterator<String> itr = moves.iterator();
        while (itr.hasNext()) {
            String move = itr.next();
            valid = game.playermoves(move);
        }
         
        return valid;
    }
 
}