package quoridor;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Quoridor {

	public static void main(String[] args) throws IOException {
		// Flag for game completion. Set to 1 or 2 on completion.
		int winner = 0; 
		Board board = new Board();
		String input;
		String[] player;
		board.display("");
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

		while ((input = stdin.readLine()) !=null && winner == 0) {
			//remove any thing with ("1. " and "2. " and etc)
			Pattern replace = Pattern.compile("\\d+\\.\\s*");
			Matcher matcher = replace.matcher(input);
			input = matcher.replaceAll("");

			//split the input into array using delimiter
			String delimiter = " ";
			player = input.split(delimiter);

			for (int j = 0; j < player.length; j++) {
				//use regex to check input
				if (player[j].matches("[a-i]{1}[1-9]{1}[h|v]?")){
					//valid move if the input match the regex
					board.display(player[j]);
					// Check for victory
					winner = board.getWinner();
					if (winner!=0) { 
						// If the game is over stop executing moves from this command.
						break;
					}
				}
				else {
					//invalid move if the input does not match the regex
					System.out.println("Invalid move");
					board.display("");
				}
			}
		}
	}
}