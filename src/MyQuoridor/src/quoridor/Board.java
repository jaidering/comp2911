package quoridor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Board implements Display{
	static int Board_Size = 9;
	int turns = 0;
	private HashMap <Tiles,Orientation> gamewall = new HashMap<Tiles, Orientation>();

	Tiles player1pos = new Tiles("e9");
	Tiles player2pos = new Tiles("e1");
	ArrayList <Walls> player1walls = new ArrayList<Walls>();
	ArrayList <Walls> player2walls = new ArrayList<Walls>();
	boolean over = false;

	@Override
	public void display(String moves) {
		System.out.println("Turn:" + turns);
		if(isOver() == false){
			playermoves(moves);

			for (char c = 'a'; c < 'j'; c++) {
				System.out.print("   "+c);
			}
			System.out.println();


			//print out the board of size 9x9
			//every mod2 == 0 is a tile
			//every mod2 == 1 is a tile separator
			//if i and j == 0 print the coordinates
			for (int i = 0; i < 2*Board_Size+1; i++) {
				for (int j = 0; j < 2*Board_Size+1; j++) {
					if (j == 0) {
						if (i%2 == 0) {
							System.out.print(" ");
						}
						else {
							System.out.print((i+1)/2);
						}
					}
					printboard(i,j);
				}
			}
		}

		turns++;

	}
	private char player(int i, int j) {
		char player1 = 'A';
		char player2 = 'B';
		Tiles playersPos = new Tiles((i-1)>>1,(j-1)>>1);

		if (player1pos.equals(playersPos)) {
			return player1;
		}           
		else { 
			return player2;
		}
	}

	private boolean hasPlayer(int i, int j) {
		Tiles playersPos = new Tiles((i-1)>>1,(j-1)>>1);

		if (player1pos.equals(playersPos) || player2pos.equals(playersPos)) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean validator(String move) {
		boolean isValid = true;

		if (move.length() == 2) {
			if (turns % 2 == 0) {
				isValid = isValidMove(move, player1pos, player2pos);
			}
			else if (turns % 2 == 1) {
				isValid = isValidMove(move, player1pos, player2pos);
			}

		}
		else if (move.length() == 3) {
			isValid = isValidWall(new Walls(move));
		}

		return isValid;
	}

	private boolean isValidMove(String moves, Tiles playerpos, Tiles otherplayerpos) {
		boolean isValidMove = false;

		int curX = playerpos.getColumn();
		int curY = playerpos.getRow();
		int otherX = otherplayerpos.getColumn();
		int otherY = otherplayerpos.getRow();
		int moveX = moves.charAt(1) - '1';
		int moveY = moves.charAt(0) - 'a';
		Tiles newMove = new Tiles(moves);

		//move right
		if ((moveX - curX == 0) && (moveY - curY == 1) && (!newMove.equals(otherplayerpos)) &&(!newMove.equals(playerpos))) {
			String inValid1 = "" + (moveY-1) + moveX + 'v';
			String inValid2 = "" + (moveY-1) + (moveX-1) + 'v';
			//check for vertical walls
			if (!player1walls.toString().contains(inValid1) && !player1walls.toString().contains(inValid2) && !player2walls.toString().contains(inValid1) && !player2walls.toString().contains(inValid2)) {
				isValidMove = true;
			}
		}

		//move left
		if ((moveX - curX == 0) && (moveY - curY == -1) && (!newMove.equals(otherplayerpos)) &&(!newMove.equals(playerpos))) {
			String inValid1 = "" + newMove + 'v';
			String inValid2 = "" + moveY + (moveX-1) + 'v';
			//check for vertical walls
			if (!player1walls.toString().contains(inValid1) && !player1walls.toString().contains(inValid2) && !player2walls.toString().contains(inValid1) && !player2walls.toString().contains(inValid2)) {
				isValidMove = true;
			}
		}

		//move down
		if ((moveX - curX == 1) && (moveY - curY == 0) && (!newMove.equals(otherplayerpos)) &&(!newMove.equals(playerpos))) {
			String inValid1 = "" + moveY + (moveX-1)+ 'h';
			String inValid2 = "" + (moveY-1) + (moveX-1) + 'h';
			//check for horizontal walls
			if (!player1walls.toString().contains(inValid1) && !player1walls.toString().contains(inValid2) && !player2walls.toString().contains(inValid1) && !player2walls.toString().contains(inValid2)) {
				isValidMove = true;
			}
		}

		//move up
		if ((moveX - curX == -1) && (moveY - curY == 0) && (!newMove.equals(otherplayerpos)) &&(!newMove.equals(playerpos))) {
			String inValid1 = "" + newMove + 'h';
			String inValid2 = "" + (moveY-1) + moveX + 'h';
			//check for horizontal walls
			if (!player1walls.toString().contains(inValid1) && !player1walls.toString().contains(inValid2) && !player2walls.toString().contains(inValid1) && !player2walls.toString().contains(inValid2)) {
				isValidMove = true;
			}
		}

		//jump right
		if ((moveX - curX == 0) && (moveY - curY == 2)) {
			if ((curX - otherX == 0) && (curY - otherY == -1)) {
				isValidMove = true;
			}
		}

		//jump left
		if ((moveX - curX == 0) && (moveY - curY == -2)) {
			if ((curX - otherX == 0) && (curY - otherY == 1)) {
				isValidMove = true;
			}
		}

		//jump down
		if ((moveX - curX == 2) && (moveY - curY == 0)) {
			if ((curX - otherX == -1) && (curY - otherY == 0)) {
				isValidMove = true;
			}
		}

		//jump up
		if ((moveX - curX == -2) && (moveY - curY == 0)) {
			if ((curX - otherX == 1) && (curY - otherY == 0)) {
				isValidMove = true;
			}
		}

		//jump up left
		if ((moveX - curX == 1) && (moveY - curY == 1)) {
			if ((curX - otherX == 0) && (curY - otherY == -1)) {
				isValidMove = true;
			}
			else if ((curX - otherX == -1) && (curY - otherY == 0)) {
				isValidMove = true;
			}
		}

		//jump up right
		if ((moveX - curX == -1) && (moveY - curY == 1)){
			if ((curX - otherX == 0) && (curY - otherY == -1)) {
				isValidMove = true;
			}
			else if ((curX - otherX == 1) && (curY - otherY == 0)) {
				isValidMove = true;
			}
		}

		//jump down left
		if ((moveX - curX == 1) && (moveY - curY == -1)) {
			if ((curX - otherX == 0) && (curY - otherY == 1)) {
				isValidMove = true;
			}
			else if ((curX - otherX == -1) && (curY - otherY == 0)) {
				isValidMove = true;
			}
		}

		//jump down right
		if ((moveX - curX == -1) && (moveY - curY == -1)){
			if ((curX - otherX == 0) && (curY - otherY == 1)) {
				isValidMove = true;
			}
			else if ((curX - otherX == 1) && (curY - otherY == 0)) {
				isValidMove = true;
			}
		}

		return isValidMove;
	}

	private boolean isValidWall (Walls wall) {
		boolean isValidWall = true;

		//wall is outside or on border
		if (wall.getColumn() >= 8 || wall.getRow() >= 8) {
			return false;
		}


		//wall at same position as other walls
		if (player1walls.toString().contains(wall.toString())) {
			return false;
		}
		if (player2walls.toString().contains(wall.toString())) {
			return false;
		}       

		//wall beside another wall (example e3h and f3h is invalid)
		if (wall.orientation == Orientation.horizontal) {
			//example d3h and e3h and f3h invalid
			String plus = "" + (wall.getRow() + 1) + wall.getColumn() + wall.toString().charAt(2);
			String minus = "" + (wall.getRow() - 1) + wall.getColumn() + wall.toString().charAt(2);
			if (player1walls.toString().contains(plus) || player1walls.toString().contains(minus)) {
				return false;
			}
			if (player2walls.toString().contains(plus) || player2walls.toString().contains(minus)) {
				return false;
			}
		}
		else if (wall.orientation == Orientation.vertical) {
			//example e2v and e3v and e4v invalid
			String plus = "" + wall.getRow() + (wall.getColumn() + 1) + wall.toString().charAt(2);
			String minus = "" + wall.getRow() + (wall.getColumn() - 1) + wall.toString().charAt(2);
			if (player1walls.toString().contains(plus) || player1walls.toString().contains(minus)) {
				return false;
			}
			if (player2walls.toString().contains(plus) || player2walls.toString().contains(minus)) {
				return false;
			}
		}

		//wall intersect another wall
		if (wall.orientation == Orientation.horizontal) {
			String opposite = "" + wall.toString().charAt(0) + wall.toString().charAt(1) + 'v';
			if (player1walls.toString().contains(opposite)) {
				return false;
			}
			if (player2walls.toString().contains(opposite)) {
				return false;
			}
		}
		else if (wall.orientation == Orientation.vertical) {
			String opposite = "" + wall.toString().charAt(0) + wall.toString().charAt(1) + 'h';
			if (player1walls.toString().contains(opposite)) {
				return false;
			}
			if (player2walls.toString().contains(opposite)) {
				return false;
			}
		}

		//wall does not block player path
		//add temp wall first to check hasPathToGoal
		int i = (wall.getColumn()+1)<<1;
		int j = (wall.getRow()+1)<<1;

		if (wall.getOrientation() == Orientation.horizontal) {
			Tiles t1 = new Tiles(i, j+1);
			Tiles t2 = new Tiles(i, j-1);
			gamewall.put(t1, wall.getOrientation());
			gamewall.put(t2, wall.getOrientation());

			//if hasPathToGoal returns false, isValidWall is false
			isValidWall = hasPathToGoal();
			//remove temp wall
			gamewall.remove(t1);
			gamewall.remove(t2);
		}
		else {
			Tiles t1 = new Tiles(i+1, j);
			Tiles t2 = new Tiles(i-1, j);
			gamewall.put(t1, wall.getOrientation());
			gamewall.put(t2, wall.getOrientation());

			//if hasPathToGoal returns false, isValidWall is false
			isValidWall = hasPathToGoal();
			//remove temp wall
			gamewall.remove(t1);
			gamewall.remove(t2);
		}
		return isValidWall;
	}
	public boolean hasPathToGoal() {
		if (!shortestPath(player1pos, 0).isEmpty()) {
			return false;
		}
		else if (!shortestPath(player2pos, 8).isEmpty()) {
			return false;
		}
		else{
			return true;
		}
	}

	protected List<Tiles> shortestPath (Tiles src, int goal) {
		List <Tiles> path = new LinkedList <Tiles>();
		Queue <Tiles> queue = new LinkedList <Tiles>();
		HashMap <Tiles, Tiles> parent = new HashMap <Tiles, Tiles>();

		//Add start to queue
		queue.add(src);
		//start has no parent
		parent.put(src, null);

		while (!queue.isEmpty()) {
			Tiles tmp = queue.poll();
			if (tmp.getRow() == goal) {
				while (!tmp.equals(src)) {
					path.add(tmp);
					tmp = parent.get(tmp);
				}
				Collections.reverse(path);
				return path;
			}
			for (Tiles move : tmp.possibleMoves(1)) {
				if (isValidMove(move.toString(), player1pos, player2pos)) {
					if (!parent.containsKey(move)) {
						parent.put(move, tmp);
						queue.add(move);
					}
				}
			}
		}
		return path;
	}
	private boolean hasWall(int i, int j) {
		//Using hash map key to check if the board separator has a wall.
		return gamewall.containsKey(new Tiles(i, j));
	}

	public boolean playermoves(String moves) {        
		boolean valid = false;
		if (moves != "") {
			if (moves.length() == 2) {
				//if moves length is 2 (example e8), it means the player has move to a tile.
				if (turns % 2 == 1) {
					if (isValidMove(moves, player1pos, player2pos) == true){
						System.out.println("Player1 moves: " + moves);
						player1pos = new Tiles(moves);
						valid = true;
					}
					else {
						System.out.println("Player1 moves: Invalid");
						turns--;
					}
				}
				else {
					if (isValidMove(moves, player2pos, player1pos) == true) {
						System.out.println("Player2 moves: " + moves);
						player2pos = new Tiles(moves);
						valid = true;
					}
					else {
						System.out.println("Player2 moves: Invalid");
						turns--;
					}
				}
			}
			else if (moves.length() == 3) {
				//when moves length is 3 (example e8v), it means add a wall 
				//Add walls into array so it does not disappear
				//each player only can place only 10 walls maximum
				if (turns % 2 == 1) {
					if (player1walls.size() < 10) {
						if (isValidWall(new Walls(moves)) == true) {
							System.out.println("Player1 add wall: " + moves);
							player1walls.add(new Walls(moves));
							valid = true;
						}
						else {
							System.out.println("Player1 add wall: Invalid");
							turns--;
						}
					}
					else {
						System.out.println("Player 1 has used up all walls.");
						turns--;
					}
				}
				else {
					if (player2walls.size() < 10) {
						if (isValidWall(new Walls(moves)) == true) {
							System.out.println("Player2 add wall: " + moves);
							player2walls.add(new Walls(moves));
							valid = true;
						}
						else {
							System.out.println("Player2 add wall: Invalid");
							turns--;
						}
					}
					else {
						System.out.println("Player 2 has used up all walls");
						turns--;
					}
				}
			}
		}
		else {
			//Do Nothing when moves is empty string

		}

		for (Walls player1wall : player1walls) {
			int i = (player1wall.getColumn()+1)<<1;
			int j = (player1wall.getRow()+1)<<1;

			if (player1wall.getOrientation() == Orientation.horizontal) {
				gamewall.put(new Tiles(i, j+1), player1wall.getOrientation());
				gamewall.put(new Tiles(i, j-1), player1wall.getOrientation());
			}
			else {
				gamewall.put(new Tiles(i+1, j), player1wall.getOrientation());
				gamewall.put(new Tiles(i-1, j), player1wall.getOrientation());
			}
		}
		for (Walls player2wall : player2walls) {
			int i = (player2wall.getColumn()+1)<<1;
			int j = (player2wall.getRow()+1)<<1;

			if (player2wall.getOrientation() == Orientation.horizontal) {
				gamewall.put(new Tiles(i, j+1), player2wall.getOrientation());
				gamewall.put(new Tiles(i, j-1), player2wall.getOrientation());
			}
			else {
				gamewall.put(new Tiles(i+1, j), player2wall.getOrientation());
				gamewall.put(new Tiles(i-1, j), player2wall.getOrientation());
			}
		}
		return valid;
	}

	public boolean isOver() {
		if(getWinner() == 1){
			over = true;
		}
		else if(getWinner() == 2){
			over = true;
		}	
		return over;
	}

	public int getWinner() {
		if(player1pos.getColumn() == 0){
			System.out.println("Player 1 wins!");
			return 1;
		}else if(player2pos.getColumn() == 8){
			System.out.println("Player 2 wins!");
			return 2;
		}
		else
			return 0;
	}

	private void printboard(int i, int j) {
		// prints the board in ASCII
		if ((i+j)%2 == 0) {
			//prints the tile separator
			if (j%2 == 0) {
				System.out.print("+");
			}
			else {
				//if there is a player on tile
				if (hasPlayer(i,j)) {
					System.out.print(" "+player(i,j)+" ");
				} 
				else {
					System.out.print("   ");
				}
			}
		}
		else {
			//print horizontal wall if there is
			if (i%2 == 0) {
				if (hasWall(i,j)) {
					System.out.print("---");
				}
				else {
					System.out.print("   ");
				}
			}
			//print vertical wall if there is
			else {
				if (hasWall(i,j)) {
					System.out.print("|");
				}
				else {
					System.out.print(" ");
				}
			}
		}

		if (j == 2*Board_Size) {
			System.out.println();
		}
	}

}