package quoridor;
public class Walls {
 
    Tiles tile = new Tiles();
    Orientation orientation = null;
    int xAxis;
    int yAxis;
     
    public Walls (Tiles topRight, Orientation orientation){
        this.tile = topRight;
        this.orientation = orientation;
    }
     
    public Walls (String s) {
         
        this.yAxis = s.charAt(0) - 'a';
        this.xAxis = s.charAt(1) - '1';
         
        if (s.length() == 3) {
            if (s.charAt(2) == 'h') {
                this.orientation = Orientation.horizontal;
            }
            else if (s.charAt(2) == 'v') {
                this.orientation = Orientation.vertical;
            }
            else {
                //Invalid orientation
            } 
        } 
    }
     
    public Orientation getOrientation() {
        return orientation;
    }
     
    public int getRow() {
        return this.yAxis;
    }
     
    public int getColumn() {
        return this.xAxis;
    }
     
    @Override
    public int hashCode() {
        return orientation.ordinal() * tile.hashCode();
    }
     
    @Override
    public String toString() {
        char ori = orientation.name().toLowerCase().charAt(0);
        int row = this.xAxis;
        int column = this.yAxis;
        return ""+column+row+ori;
    }
}