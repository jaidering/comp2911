package lab06;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LinkedListAtLastTest {
	
	protected static List	list;
	
	@Before
	public void setUp() throws Exception {
		list = new LinkedListAtLast();
	}
	
	
	Object	item1	= "aString";
	Object	item2	= "anotherString";
	Object	item3	= "yetAnotherString";
	

	@Test
	public void testPrint() {
		list.add(item1);
		list.add(item2);
		System.out.println("Output:....");
		list.print();
	}
	@Test
	public void testNext(){
		list.add(item1);
		assertEquals("",item1,list.iterator().next());
		
	}

	@Test
	public void testhasNext() {
		list.add(item1);
		assertTrue("Has next", list.iterator().hasNext());
		
	}
	
	@Test
	public void hasMultipleHasNext(){
		for(int i = 0; i<list.size();i++){
			if(i%2==1){
			list.add(item1);
			assertTrue("Has next", list.iterator().hasNext());
			}
			else if (i%2 == 0){
				list.add(item2);
				assertTrue("Has next", list.iterator().hasNext());
			}
			else if (i ==list.size()){
				assertFalse("Has no next", list.iterator().hasNext());
			}
		}
	}
	
	@Test (expected = UnsupportedOperationException.class)
	public void remove(){
		list.iterator().remove();
		
	}

}
