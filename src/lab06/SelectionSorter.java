package lab06;

import java.util.Comparator;

public class SelectionSorter implements Sorter {

	@Override
	public <E extends Comparable<? super E>> void sort(E[] data) {
		int minIndex;
		for (int i = 0 ; i < data.length ; i++) {
			minIndex = i;
			for (int j = i+1 ; j < data.length ; j++) {
				if (data[minIndex].compareTo(data[j]) == 1)
					minIndex = j;
			}
			if (minIndex != i)
				swap(data, i, minIndex);
		}

	}

	@Override
	public <E> void sort(E[] data, Comparator<? super E> comp) {
		int minIndex;
		for (int i = 0 ; i < data.length ; i++) {
			minIndex = i;
			for (int j = i+1 ; j < data.length ; j++) {
				if (comp.compare(data[minIndex], data[j]) == 1)
					minIndex = j;
			}
			if (minIndex != i)
				swap(data, i, minIndex);
		}

	}

	private <E> void swap (E[] data, int i, int j) {
		E temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}

}