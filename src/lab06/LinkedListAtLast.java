package lab06;

import java.util.Iterator;
import java.lang.Object;

/**
 * Linked list implementation. Add method adds to end of list.
 * 
 * @author John Potter
 * 
 */
public class LinkedListAtLast implements List {

	private int size;
	private Link first;

	public void add(Object o) {
		if (size == 0)
			first = new Link(o, null);
		else
			new Link(lastLink(), o);
		++size;
	}

	public boolean find(Object o) {
		for (Link current = first; current != null; current = current.getNext())
			if (current.getItem().equals(o))
				return true;
		return false;
	}

	public void print() {
		Link current = first;
		for (int i = 0; i < size; ++i) {
			System.out.println("" + i + ":" + current.getItem());
			current = current.getNext();
		}
	}

	public int size() {
		return size;
	}

	public Object getLast() {
		if (size == 0)
			throw new IllegalStateException("empty list");
		assert size > 0 : "list must be non-empty";
		return lastLink().getItem();
	}

	private Link lastLink() {
		Link current = null;
		Link next = first;
		while (next != null) {
			current = next;
			next = next.getNext();
		}
		return current;
	}

	// annonynomous class
	public class LinkListItertor implements Iterator<Object> {

		private Link current;

		public LinkListItertor(Link l) {
			current = l;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			if (current != null) {
				return true;
			} else
				return false;
		}

		@Override
		public Object next() {
			Object o = current.getItem();
			current = current.getNext();
			return o;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException(
					"Remove method is not supported");
		}
	}

	@Override
	public Iterator<Object> iterator() {
		// TODO Auto-generated method stub

		return new LinkListItertor(first);
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub

		if ( this == obj ) return true;
		if ( !(obj instanceof LinkedListAtLast) ) 
			return false;
		LinkedListAtLast that = (LinkedListAtLast) obj;
		if (this.size!=that.size) 
			return false;
		boolean equals = true;
		Iterator<Object> thisIt = this.iterator();
		Iterator<Object> thatIt = that.iterator();
		while (thisIt.hasNext()&&thatIt.hasNext())
			equals &= thisIt.next().equals(thatIt.next());
		return equals;
	}

	// TODO Auto-generated method stub
	// Good way to do is get hash for each object then add all the number to get
	// another numb
	// then we can check with the different list for the number

	@Override
	public int hashCode() {
		int hashCode = 1;
		// Linear hashing algorithm. Becomes quadratic when hashing a list of lists
		for (Object obj : this)
			hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
		return hashCode;
	}

	@Override
	public String toString() {
		StringBuilder sd = new StringBuilder();
		for (Object e: this)
			sd.append("["+e+"], ");
		return sd.toString();
	}

}
