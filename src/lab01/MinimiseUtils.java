package lab01;

public class MinimiseUtils {

	/**
     * Minimum of two integers.
     * @param i first of two integers
     * @param j second of two integers
     * @return minimum of i and j
     */
    static int min2(int i, int j) {
        return i < j ? i : j;
    }

    static int min3(int i, int j, int k) {
        int temp = min2(j, k);
        int result = min2(i, temp);
        return result;
    }

    static int min(int[] data) {
        return min(data, 0, data.length - 1);
    }
    
    static int min1(int[]data){
    	
    	int minimum = data[0];
    	int i = 1;
    	
    	while( i<data.length)
    	{
    		if (data[i] < minimum){
    			minimum = data[i];
    		}
    		i++;
    	}
    	return minimum;
    }

    private static int min(int[] data, int start, int end) {
        return start == end ?
                data[start] :
                start > end ?
                    Integer.MAX_VALUE :
                    min2(data[start], min(data, start + 1, end));
    }

    public static void main(String[] args) {
        int i = 99;
        int j = 55;
        int k = 11;
        System.out.print("Minimum of " + i + ", " + j + ", " + k + " is ");
        System.out.println(min3(i, j, k));

        int[] data = {45, 23, 65, 24, 36, 63, 62, 15};
        System.out.println("Minimum of test data array is " + min(data));
        
        int[] data2 = {45, 23, 65, 24, 36, 63, 62, 15, 48, 72, 2};
        System.out.println("Minimum of test data array is " + min1(data2));


	}

}
