package lab10;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class DijkstraSolver implements SlidingBlockSolver {

	@Override
	public int[] solve(int[] start, int[] goal, int maxMoves) {
		PuzzleLayout startingLayout = new PuzzleLayout(start);
		PuzzleLayout goalLayout = new PuzzleLayout(goal);
		Queue<PuzzleLayout> queue = new LinkedList<PuzzleLayout>();
		HashMap <PuzzleLayout,Boolean> marker = new HashMap<PuzzleLayout, Boolean>();

		// enqueue start configuration onto queue
		queue.add(startingLayout);
		// mark start configuration
		marker.put(startingLayout, true);
		while (!queue.isEmpty()) {
			PuzzleLayout i = queue.poll();
			if (i.equals(goalLayout)) {
				if (i.moves.size() <= maxMoves) {
					return i.moves();
				} else {
					return null;
				}
			}
			for (int x: i.incidentEdges()) {
				PuzzleLayout y = i.opposite(x);
				if (!marker.containsKey(y)) {
					marker.put(y, true);
					queue.add(y);
				}
			}
		}
		return null;
	}
}