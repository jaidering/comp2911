package lab10;


import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class PuzzleLayout {

	int[] puzzle;
	int holePosition;
	int size;
	List <Integer> moves = new LinkedList<Integer>();

	public PuzzleLayout(int[]puzzle){
		this.puzzle = Arrays.copyOf(puzzle, puzzle.length);
		for(holePosition = 0;holePosition < puzzle.length && puzzle[holePosition]!=0; holePosition++){
			size = (int)Math.sqrt(puzzle.length);
		}
	}

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(puzzle);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuzzleLayout other = (PuzzleLayout) obj;
		if (!Arrays.equals(puzzle, other.puzzle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for (int i = 0; i < puzzle.length; i++) {
			sb.append(String.format("%2d ", puzzle[i]));
			if ((i+1)/size > i/size)
				sb.append("\n");
		}
		return sb.toString();
	}

	public PuzzleLayout opposite (int edge) {
		PuzzleLayout opposite = new PuzzleLayout(puzzle);
		int temp = opposite.puzzle[holePosition];
		opposite.puzzle[holePosition] = opposite.puzzle[edge];
		opposite.puzzle[edge] = temp;
		opposite.holePosition = edge;
		opposite.moves.addAll(moves);
		opposite.moves.add(edge);
		return opposite;
	}

	public List<Integer> incidentEdges() {
		LinkedList<Integer> incidentEdges = new LinkedList<Integer>();
		for (int i = -1 ; i < 2 ; i++) {
			if (i != 0) {
				// Check the column displacement will not alter the row
				if (holePosition+i >= 0 && (holePosition+i)/size == holePosition/size) {
					incidentEdges.add(holePosition+i);
				}
				if (holePosition+i*size >= 0 && holePosition+i*size < puzzle.length) {
					incidentEdges.add(holePosition+i*size);
				}
			}
		}
		return incidentEdges;
	}

	public List<PuzzleLayout> neighbors () {
		LinkedList<PuzzleLayout> neighbors = new LinkedList<PuzzleLayout>();
		for (int e: incidentEdges()) {
			neighbors.add(opposite(e));
		}
		return neighbors;
	}

	public int[] moves() {
		int[] moves = new int[this.moves.size()];
		Iterator<Integer> itr = this.moves.iterator();
		for (int i = 0; itr.hasNext(); i++) {
			moves[i] = itr.next();
		}
		return moves;
	}

	public int heuristic () {
		int sum = 0;
		for (int i = 0 ; i < puzzle.length; i++) {
			if (puzzle[i] != 0) {
				sum += Math.abs(puzzle[i]/size-i/size)+Math.abs(puzzle[i]%size-i%size);
			}
		}
		return sum;
	}

	public int heuristic (PuzzleLayout goal) {
		int sum = 0;
		for (int i = 0 ; i < puzzle.length; i++) {
			if (puzzle[i] != 0) {
				sum += Math.abs(puzzle[i]/size-goal.puzzle[i]/size)+Math.abs(puzzle[i]%size-goal.puzzle[i]%size);
			}
		}
		return sum;
	}

}
