package lab10;

import java.util.Comparator;

public class MinimalCostComparator implements Comparator<PuzzleLayout> {

	@Override
	public int compare(PuzzleLayout a, PuzzleLayout b) {
		Integer sizeA = a.moves.size();
		Integer sizeB = b.moves.size();
		return sizeA.compareTo(sizeB);
	}
}