package lab10;

import java.util.HashMap;

public class IteratedDepthFirstSolver implements SlidingBlockSolver {

	HashMap <PuzzleLayout,Boolean> marker = new HashMap<PuzzleLayout,Boolean>();

	@Override
	public int[] solve (int[] start, int[] goal, int maxMoves) {
		for (int depth = 0; depth < maxMoves; depth++) {
			int[] result = DLS(new PuzzleLayout(start), new PuzzleLayout(goal), depth);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	private int[] DLS(PuzzleLayout node, PuzzleLayout goal, int depth) {
		System.out.println(node);
		if (depth == 0 && node.equals(goal)) {
			return node.moves();
		} else if (depth > 0) {
			for (PuzzleLayout e:node.neighbors()) {
				int[] temp = DLS(e, goal, depth-1);
				if (temp != null) {
					return temp;
				}
			}
		}
		return null;
	}

}