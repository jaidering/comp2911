package lab10;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstSolver implements SlidingBlockSolver{

	@Override
	public int[] solve(int[] start, int[] goal, int maxMoves) {
		// TODO Auto-generated method stub

		PuzzleLayout startingLayout = new PuzzleLayout(start);
		PuzzleLayout goalLayout = new PuzzleLayout(goal);
		Queue<PuzzleLayout> queue = new LinkedList<PuzzleLayout>();
		HashMap <PuzzleLayout,Boolean> marker = new HashMap<PuzzleLayout, Boolean>();

		//add initial layout
		queue.add(startingLayout);
		//mark starting layout
		marker.put(startingLayout,true);
		while(!queue.isEmpty()){
			PuzzleLayout x = queue.poll();
			
			if(x.equals(goalLayout)){
				if(x.moves.size()<=maxMoves){
					return x.moves();
				}
				else{
					return null;
				}
			}
			for(int i:x.incidentEdges()){
				PuzzleLayout y = x.opposite(i);
				
				if(!marker.containsKey(y)){
					marker.put(y,true);
					queue.add(y);
				}
			}
		}
		return null;
	}

}
