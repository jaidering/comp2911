package lect06;

import java.util.List;
import java.util.ArrayList;

public class Box <E>{
List <E> elements = new ArrayList<E>();
void add(E e)
{
	elements.add(e);	
}

E get()
{
	return elements.get(0);
}
@Override
public boolean equals(Object obj) {
	// TODO Auto-generated method stub

	if(obj instanceof Box <?>){
		@SuppressWarnings("unchecked")
		Box <E> that = (Box<E>) obj;
		return this.equalBox(that);
	}
	return super.equals(obj);
	}

private boolean equalBox(Box<E> that) {
	// TODO Auto-generated method stub
	return elements.equals(that.elements);
}
 public static void main (String [] args){
	 
	 Box <String> bs = new Box<String>();
	 bs.add("one");
	 bs.add("two");
	 
	 Box <Integer> bi = new Box<Integer>();
	 bi.add(1);
	 bi.add(2);
	 
	 assert(bs.equals(bi));
	 assert(bi.equals(bs));
//	 assert(bs.equalBox(bi));
 }

}
