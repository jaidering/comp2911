package lab02;

public class NanoTimer implements Timer
{
	private boolean isRunning;
	private boolean paused;
    private long startTimer = 0 ;
    private long stopTimer = 0;
    private long pauseTime=0;
    private long resume=0;
    private long elapTime=0;
    private long durationOfpause = 0 ;

    

	public static long currentTime ()
	{
		return System.nanoTime ();
	}
	
	// TODO add your own fields for recording Timer state and time information here
	

	public void start ()
	{
		
		startTimer = currentTime();
		isRunning = true;

	}

	public Interval elapsed ()
	{	

		    if (startTimer == 0)
		    {
		      elapTime = 0 ;
		    }		    
		    else if((isRunning==true)&&(paused == false))
		    {
			  elapTime = currentTime() - startTimer - durationOfpause ;
		    }
		    else if((isRunning==true)&&(paused == true))
		    {
		      elapTime= pauseTime - startTimer - durationOfpause ;
		    }
		    else
		    {
		    	elapTime = stopTimer - startTimer - durationOfpause ;	
		    }
            Interval p = new NanoInterval(elapTime) ;
            return p;
			
	}
	
	public void stop ()
	{
		// TODO Auto-generated method stub	
	  if(isRunning == true)
	  {
	    stopTimer = currentTime(); 	 
	    isRunning = false ;

	  }
	}

	public void suspend ()
	{
		// TODO Auto-generated method stub
		if(paused==false)
		{
		 pauseTime = currentTime();
		 paused = true ;

		}
	}

	public void resume ()
	{
		// TODO Auto-generated method stub
     if(paused == true)
     {
		 resume= currentTime();
		 
		 if(startTimer!=0)
		 {
			 Interval i = new NanoInterval(0);
			 i=i.add (pauseTime,resume);
			 durationOfpause=i.nanoSeconds() + durationOfpause;
		 }		
		 paused = false ;

     }
	  
	}
	
}