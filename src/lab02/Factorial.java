package lab02;

import java.math.BigInteger;

public class Factorial
{
	int	n;
	
	public static void require (String message, boolean condition)
	{
		if (!condition)
			throw new IllegalArgumentException ("require " + message);
	}
	
	
	public static boolean validIndex (int n)
	{
		return n >= 0;
	}
	public Factorial (int n)
	{
		// TODO
		this.n = n;
		require ("Nonnegative index", validIndex (n));
	}
	
	public int asInt ()
	{
		// TODO
		int result = 1;
		for (int i = 2; i <= n; ++i)
			result *= i;
		return result;
	}
	
	public long asLong ()
	{
		// TODO
		long result = 1;
		for (int i = 2; i <= n; ++i)
			result *= i;
		return result;
	}
	
	public float asFloat ()
	{
		// TODO
		float result = 1;
		for (int i = 2; i <= n; ++i)
			result *= i;
		return result;
	}
	
	public double asDouble ()
	{
		// TODO
		double result = 1;
		for (int i = 2; i <= n; ++i)
			result *= i;
		return result;
	}
	
	public BigInteger asBigInteger ()
	{
		// TODO
		BigInteger result = BigInteger.ONE;
		for (int i = 2; i <= n; ++i)
			result = result.multiply (BigInteger.valueOf (i));
		return result;
	}
}
