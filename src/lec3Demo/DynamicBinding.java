package lec3Demo;

public class DynamicBinding {
static class A {
	@Override
	public String toString(){
		return "A";
	}
	static class B extends A {
		@Override
		public String toString(){
			return "B";
		}
	
		static void useA(A a){
			System.out.println(a.toString());			
		}
		
		public static void main(String []args){
			useA(new A());
			useA(new B());
		}
		}
	
}
}
